var builder = DistributedApplication.CreateBuilder(args);

builder.AddProject<Projects.UserService_Web>("userservice-web");

builder.AddProject<Projects.ApiGatewayOcelot>("apigatewayocelot");

builder.AddProject<Projects.OrderService_Web>("orderservice-web");

builder.AddProject<Projects.IdentityService_Web>("identityservice-web");

builder.Build().Run();
